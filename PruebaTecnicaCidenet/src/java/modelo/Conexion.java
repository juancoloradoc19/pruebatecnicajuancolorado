//CONEXION CON LA BASE DE DATOS
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
    
    
    private final String base = "Usuarios";
    private final String usuario = "user";
    private final String clave = "1234";
    private final String url = "jdbc:mysql://localhost:3306"+base;    
    Connection con;
        
        public Conexion() throws SQLException{
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+base);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
}
