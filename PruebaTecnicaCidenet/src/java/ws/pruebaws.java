package ws;

import java.sql.SQLException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
 


@WebService(serviceName = "pruebaws")
public class pruebaws{
  
    

    @WebMethod(operationName = "registrarUsuario")
    public int registrarUsuario(@WebParam(name = "nombreCompleto") String nombreCompleto, @WebParam(name = "tipoDocumento") String tipoDocumento, @WebParam(name = "numeroDocumento") int numeroDocumento) throws SQLException
    {
        /*Se inicializa la variable a retornar en 0, al igualque variables que se utilizarán para validar los datos.*/
        int resp = 0;
        boolean state = true;
        String numdoccadena = Integer.toString(numeroDocumento);
        
        //metodo para validar que no se escriban numeros en la variable de nombre
        for( int i = 0; i < nombreCompleto.length(); i++ )
        {
         if(Character.isDigit( nombreCompleto.charAt( i ) ) )
            state = false;
        }
        
        /*Verificación del campo nombre*/
        if(state == false || nombreCompleto.length() > 100 || nombreCompleto.replace(" ","").length() == 0){
            resp = 1;
        }
        
        /*Validación del campo tipo de documento*/
        if(!"CC".equals(tipoDocumento) && !"TI".equals(tipoDocumento))
        {
           resp = 2;
        }
        
        /*Validacion del campo documento*/
        if(numeroDocumento <= 0 || numdoccadena.length() < 10)
        {
            resp = 3;
        }        

        //RETORNAR EL CODIGO 0 SI SE REALIZA CON EXITO EL INSERT EN LA BASE DE DATOS
        return resp;
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarUsuario")
    public int consultarUsuario(@WebParam(name = "tipoDocumento") String tipoDocumento, @WebParam(name = "numeroDocumento") int numeroDocumento) {
        //VALIDAR QUE LOS DATOS TENGAN EL FORMATO ADECUADO
        int resp = 0;
        String numdoccadena = Integer.toString(numeroDocumento);
        
        /*Validación del campo tipo de documento*/
        if(!"CC".equals(tipoDocumento) && !"TI".equals(tipoDocumento))
        {
           resp = 2;
        }
        
        /*Validacion del campo documento*/
        if(numeroDocumento <= 0 || numdoccadena.length() < 10)
        {
            resp = 3;
        } 
        
        //RETORNAR EL CODIGO 0 SI SE ENCUENTRA EL USUARIO, E IMPRIMIR EL NOMBRE
        return resp;
    }
}
