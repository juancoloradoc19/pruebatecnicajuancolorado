package Principal;

import ws.Pruebaws;
import ws.Pruebaws_Service;
import java.util.Scanner;


public class test1 {

    public static void main(String[] args) {

        //Creacion y definición de variables globales para el funcionamiento de la aplicación.
        Pruebaws_Service servicio = new Pruebaws_Service();   
        Pruebaws cliente = servicio.getPruebawsPort();
        Scanner entrada = new Scanner(System.in);
        boolean state2 = false;
        
        String doc,numdoc,nom;
        int menu;
        
        
        do{
            System.out.println(" ___________________________________");
            System.out.println("|                                   |");
            System.out.println("| BIENVENIDO AL MENU DE OPERACIONES |");
            System.out.println("|___________________________________|");
            System.out.println("|                                   |");
            System.out.println("|    1. Registrar un usuario.       |");
            System.out.println("|    2. Consultar un usuario.       |");
            System.out.println("|    3. Salir de la aplicación.     |");
            System.out.println("|___________________________________|");
            System.out.print(" porfavor eliga una operación:     ");
            menu = entrada.nextInt();
            
            switch (menu)
            {
                case 1:{                    
                    Scanner entrada2 = new Scanner(System.in);
                    System.out.println(" _________________________________ ");
                    System.out.println("|                                 |");
                    System.out.println("|      REGISTRO DE USUARIO        |");
                    System.out.println("|_________________________________|");
                    System.out.println("");
                     
                    System.out.print("Ingrese el nombre del usuario que desea registrar (Debe ingresar solo caracteres en este campo.): ");
                    nom = entrada2.nextLine();
                    
                    System.out.print("Ingrese el tipo de documento (Solo son validos los tipos CC o TI): ");
                    doc = entrada2.nextLine();
                    
                    //CICLO PARA VERIFICAR QUE LOS DATOS QUE SE MANDEN A LAS OPERACIONES DEL WS CUMPLAN LOS TIPOS
                    do{
                        System.out.print("Ingrese el numero de documento (Valido numero de 10 digitos): ");
                        numdoc = entrada2.nextLine();
                  
                                                    
                                for( int i = 0; i < numdoc.length(); i++ )
                                    {
                                        if(Character.isDigit(numdoc.charAt( i )))
                                        {
                                            state2 = true;
                                        }else
                                        {
                                            state2 = false;
                                            break;
                                        }                                 
                                }
                            
                    }while(state2 == false || numdoc.length() > 10 || numdoc.length() == 0 );
                    

                    int numdocverificado = Integer.parseInt(numdoc);

                   
                    //LLAMAMIENTO A LA OPERACION REGISTRAR DEL WS
                    int respuesta = cliente.registrarUsuario(nom,doc,numdocverificado);
                        
                        //BLOQUE DE COGIDO DE EJECUCION DEL MEOTOD REGISTRAR
                        switch(respuesta)
                            {
                                case 0:{
                                    System.out.println("");
                                    System.out.println(" _____________________________________");
                                    System.out.println("|                                     |");
                                    System.out.println("| Código de ejecución: "+respuesta+".             |");
                                    System.out.println("| El usuario fue registrado con éxito.|");
                                    System.out.println("|_____________________________________|");
                                    System.out.println("");
                                }break;
           
                                case 1:{
                                    System.out.println("");
                                    System.out.println(" ________________________________________________________________________");
                                    System.out.println("|                                                                        |");
                                    System.out.println("| Código de ejecución: "+respuesta+".                                                 |");
                                    System.out.println("| Se presentó un error a la hora de registrar el usuario.                 |");
                                    System.out.println("| Por favor verifique que el campo nombre esta diligenciado correctamente.|");
                                    System.out.println("|________________________________________________________________________ |");
                                    System.out.println("");
                                }break;
           
                                case 2:{
                                    System.out.println("");
                                    System.out.println(" _________________________________________________________");
                                    System.out.println("|                                                         |");
                                    System.out.println("| Código de ejecución: "+respuesta+".                                 |");
                                    System.out.println("| Se presentó un error a la hora de registrar el usuario. |");
                                    System.out.println("| Verifique que el Tipo de documento sea CC o TI.         |");
                                    System.out.println("|_________________________________________________________|");
                                    System.out.println("");
                                }break;
                                
                                case 3:{
                                    System.out.println("");
                                    System.out.println(" _________________________________________________________");
                                    System.out.println("|                                                         |");
                                    System.out.println("| Código de ejecución: "+respuesta+".                                 |");
                                    System.out.println("| Se presentó un error a la hora de registrar el usuario. |");
                                    System.out.println("| Verifique que el Numero de documento sea valido.        |");
                                    System.out.println("|_________________________________________________________|");
                                    System.out.println("");
                                }break;                                
                            }
                    //BLOQUE DE EJECUCION DEL METODO REGISTRAR
                    }break;
                    
                    
                case 2:{
                    Scanner entrada2 = new Scanner(System.in);
                    System.out.println(" _________________________________ ");
                    System.out.println("|                                 |");
                    System.out.println("|      CONSULTA DE USUARIO        |");
                    System.out.println("|_________________________________|");
                    System.out.println("");
                     
                    System.out.print("Ingrese el tipo de documento (Solo son validos los tipos CC o TI): ");
                    doc = entrada2.nextLine();
                    
                    //CICLO PARA VERIFICAR QUE LOS DATOS QUE SE MANDEN A LAS OPERACIONES DEL WS CUMPLAN LOS TIPOS
                    do{
                        System.out.print("Ingrese el numero de documento (Valido numero de 10 digitos): ");
                        numdoc = entrada2.nextLine();
                  
                                                    
                                for( int i = 0; i < numdoc.length(); i++ )
                                    {
                                        if(Character.isDigit(numdoc.charAt( i )))
                                        {
                                            state2 = true;
                                        }else
                                        {
                                            state2 = false;
                                            break;
                                        }                                 
                                }
                            
                    }while(state2 == false || numdoc.length() > 10 || numdoc.length() == 0 );
                    

                    int numdocverificado = Integer.parseInt(numdoc);
                    
                    //LLAMAMIENTO A LA OPERACION REGISTRAR DEL WS
                    int respuesta = cliente.consultarUsuario(doc,numdocverificado);
                    
                        //BLOQUE DE COGIDO DE EJECUCION DEL MEOTOD CONSULTAR
                        switch(respuesta)
                            {
                                case 0:{
                                    System.out.println("");
                                    System.out.println(" _____________________________________");
                                    System.out.println("|                                     |");
                                    System.out.println("| Código de ejecución: "+respuesta+".             |");
                                    System.out.println("| El usuario existe en los registros. |");
                                    System.out.println("|_____________________________________|");
                                    System.out.println("");
                                }break;
           
                                case 2:{
                                    System.out.println("");
                                    System.out.println(" _________________________________________________________");
                                    System.out.println("|                                                         |");
                                    System.out.println("| Código de ejecución: "+respuesta+".                                 |");
                                    System.out.println("| Se presentó un error a la hora de buscar el usuario.    |");
                                    System.out.println("| Verifique que el Tipo de documento sea CC o TI.         |");
                                    System.out.println("|_________________________________________________________|");
                                    System.out.println("");
                                }break;
                                
                                case 3:{
                                    System.out.println("");
                                    System.out.println(" _________________________________________________________");
                                    System.out.println("|                                                         |");
                                    System.out.println("| Código de ejecución: "+respuesta+".                                 |");
                                    System.out.println("| Se presentó un error a la hora de buscar el usuario.    |");
                                    System.out.println("| Verifique que el Numero de documento sea valido.        |");
                                    System.out.println("|_________________________________________________________|");
                                    System.out.println("");
                                }break;                                
                            }
                    //BLOQUE DE EJECUCION DEL METODO CONSULTAR                    
                    
                    
                    
                    }break;
                    }
        }while(menu != 3);

                    System.out.println(" _________________________________ ");
                    System.out.println("|                                 |");
                    System.out.println("|      GRACIAS POR USAR NUESTRO   |");
                    System.out.println("|             SERVICIO            |");
                    System.out.println("|_________________________________|");
                    System.out.println("");        
        
    }
    
}
