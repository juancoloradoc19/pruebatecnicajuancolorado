
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarUsuario_QNAME = new QName("http://ws/", "consultarUsuario");
    private final static QName _ConsultarUsuarioResponse_QNAME = new QName("http://ws/", "consultarUsuarioResponse");
    private final static QName _RegistrarUsuario_QNAME = new QName("http://ws/", "registrarUsuario");
    private final static QName _RegistrarUsuarioResponse_QNAME = new QName("http://ws/", "registrarUsuarioResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsultarUsuario }
     * 
     */
    public ConsultarUsuario createConsultarUsuario() {
        return new ConsultarUsuario();
    }

    /**
     * Create an instance of {@link ConsultarUsuarioResponse }
     * 
     */
    public ConsultarUsuarioResponse createConsultarUsuarioResponse() {
        return new ConsultarUsuarioResponse();
    }

    /**
     * Create an instance of {@link RegistrarUsuario }
     * 
     */
    public RegistrarUsuario createRegistrarUsuario() {
        return new RegistrarUsuario();
    }

    /**
     * Create an instance of {@link RegistrarUsuarioResponse }
     * 
     */
    public RegistrarUsuarioResponse createRegistrarUsuarioResponse() {
        return new RegistrarUsuarioResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarUsuario")
    public JAXBElement<ConsultarUsuario> createConsultarUsuario(ConsultarUsuario value) {
        return new JAXBElement<ConsultarUsuario>(_ConsultarUsuario_QNAME, ConsultarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "consultarUsuarioResponse")
    public JAXBElement<ConsultarUsuarioResponse> createConsultarUsuarioResponse(ConsultarUsuarioResponse value) {
        return new JAXBElement<ConsultarUsuarioResponse>(_ConsultarUsuarioResponse_QNAME, ConsultarUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "registrarUsuario")
    public JAXBElement<RegistrarUsuario> createRegistrarUsuario(RegistrarUsuario value) {
        return new JAXBElement<RegistrarUsuario>(_RegistrarUsuario_QNAME, RegistrarUsuario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws/", name = "registrarUsuarioResponse")
    public JAXBElement<RegistrarUsuarioResponse> createRegistrarUsuarioResponse(RegistrarUsuarioResponse value) {
        return new JAXBElement<RegistrarUsuarioResponse>(_RegistrarUsuarioResponse_QNAME, RegistrarUsuarioResponse.class, null, value);
    }

}
